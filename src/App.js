import logo from "./logo.svg";
import "./App.css";
import Header from "./ui-components/Header";
import { createGlobalStyle, useTheme } from "styled-components";

function App() {
  const theme = useTheme();

  return (
    <div className="App">
      <Header red={"red"} />
      <div className="red">Sample</div>
      <GlobalStyle />
    </div>
  );
}

const GlobalStyle = createGlobalStyle`

.red {
  background-color: red;

  
}

`;

export default App;
